
var tagList = $('#tagList');
var wordField = $('input[name=phrase]');
var tips = [];
tips.push($('input[name=phrase-tip-one]'));
tips.push($('input[name=phrase-tip-two]'));
tips.push($('input[name=phrase-tip-three]'));
var activeCard = false;


initEdit();

function initEdit(){
	tagList.html('');
	loadTags();	
	bindEvents();
}

function deleteTag(id){
	removeFromArray(storageName, id);
	reloadStorage();
	$('#id_delete_'+id).parent().remove();
}

function createTag(){
	var card = constructTagObject();
	
	if(card){
		if(typeof phrases !== 'undefined' && phrases != null && phrases.hasOwnProperty(1)){
			var id = createTagID();
			card.id = id;
			addToArray(storageName, id, card);
		} else {
			card.id = 1;
			addToArray(storageName, 1, card);
		}

		reloadStorage();
		templateTag(card);
		clearFields();
	}
}

function createTagID(){
	var array = generateIDArray();
	var max = Math.max.apply(Math, array);
	var id = max + 1;
	return id;
}

function generateIDArray(){
	var ids = [];
	for(var index in phrases){
		if(phrases.hasOwnProperty(index)){
			ids.push(parseInt(index,10));
		}
	}
	return ids;
}

function reloadStorage(){
	phrases = getArray(storageName);
}

function constructTagObject(){
	var card = {};
	var word = wordField.val();
	if(word.length < 1){
		return false;
	}
	card.word = word;
	card.tips = [];
	for(i = 0; i <= 2; i++){
		card.tips.push(tips[i].val());
	}
	return card;
}

function updateTag(){
	var card = constructTagObject();
	card.id = activeCard;
	addToArray(storageName, activeCard, card);	
	$('#id_update_'+card.id).html(card.word);
	$('#id_update_'+card.id).parent().removeClass('tags__item--active');
	$('.edit__add').find('.fa').addClass('fa-tag');
	$('.edit__add').find('.fa').removeClass('fa-check');
	reloadStorage();
	clearFields();
}

function loadTag(id){
	if(activeCard){
		updateTag();
	}

	var card = getFromArray(storageName, id);
	wordField.val(card.word);
	for(i = 0; i <= 2; i++){
		tips[i].val(card.tips[i]);
	}
	activeCard = id;
	wordField.focus();
	$("html, body").scrollTop(wordField.offset().top - 20);

	$('#id_update_'+card.id).parent().addClass('tags__item--active');

	//need to change icon
	$('.edit__add').find('.fa').removeClass('fa-tag');
	$('.edit__add').find('.fa').addClass('fa-check');
}

function clearFields(){
	wordField.val('');
	for(i = 0; i <= 2; i++){
		tips[i].val('');
	}
	activeCard = false;
}

function loadTags(){
	for(var index in phrases){
		if(phrases.hasOwnProperty(index)){
			var obj = phrases[index];
			if(typeof obj === 'string'){
				obj = convertTag(obj);
				delete phrases[index];
				removeFromArray(storageName, index);
			} 
			templateTag(obj);
		}
	}
}

function bindEvents(){
	$('#addTag').bind('click', function(e){
		e.preventDefault();
		if(activeCard){
			updateTag();
		} else {
			createTag();
		}
	});

	$('#cancelChanges').bind('click', function(e){
		e.preventDefault();
		clearFields();
		$('.tags__item').removeClass('tags__item--active');
	});
}


function templateTag(card){
	var html = '';
	html +=		'<li class="tags__item">';
	html +=	        '<a id="id_update_'+card.id+'" href="#" data-id="'+card.id+'" class="tags__link">';
	html +=		        card.word;
	html +=		    '</a>';
	html +=		    '<button id="id_delete_'+card.id+'" data-id="'+card.id+'" class="tags__delete">';
	html +=		       ' <i class="fa fa-trash"></i>';
	html +=		    '</button>';
	html +=		'</li>';

	tagList.append(html);

	$('#id_update_'+card.id).bind('click', function(e){
		e.preventDefault();
		loadTag(card.id);
	});

	$('#id_delete_'+card.id).bind('click', function(e){
		deleteTag(card.id);
	});
}


function convertTag(string){
	var card = {};
	var comps = string.split('(');
	card.word = comps[0];
	card.tips = [];

	for(var i = 1; i <= comps.length; i++){
		var tip = comps[i];
		if(!!tip){
			tip = tip.slice(0, -1);
			card.tips.push(tip);
		}
	}
	if(typeof phrases !== 'undefined' && phrases != null && phrases.hasOwnProperty(1)){
		var id = createTagID();
		card.id = id;
		addToArray(storageName, id, card);
	} else {
		card.id = 1;
		addToArray(storageName, 1, card);
	}
	reloadStorage();
	return card;
}
