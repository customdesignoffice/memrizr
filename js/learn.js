var paused = true;
var showingAnswers = false;
var timer;



function initInteractions(){
	$(document).bind('keydown', function(e) {
		if (e.keyCode == 32) {
			e.preventDefault();
			if(!showingAnswers){
		  		showAnswers();
		  	} else {
		  		startTimer();
		  		$('#skip').addClass('active hover');
		  	}
		}
	});

	$(document).bind('keyup', function(e) {
		if (e.keyCode == 32) {
			if(!showingAnswers){
		  		$('#skip').removeClass('active hover');
		  	}
		}
	});

	$('.phrase').bind('click', function(e){
		if(!showingAnswers){
	  		showAnswers();
	  	} else {
	  		hideAnswers();
	  	}
	});

	$('#skip').on('click', function(e){
		startTimer();
	});

	$('.tools__link--menu').bind('click', function(e){
		pauseTimer();
	});

	$('#edit').bind('click', function(e){
		pauseTimer();

		$('.main').toggleClass('main--hidden');
		$('.edit').toggleClass('edit--hidden');
	})
}

function initPause(){
	if(scrollRate > 0){
		paused = false;
	} else {
		paused = true;
	}
	updatePauseIcon();
}

function pauseTimer(displayed){
	paused = true;
	clearTimeout(timer);
	updatePauseIcon();
}

function updatePauseIcon(){
	if(paused){
		$('.pause-indicator').addClass('pause-indicator--active');
		$('#skip').addClass('controls__action--pulse');
	} else {
		$('.pause-indicator').removeClass('pause-indicator--active');
		$('#skip').removeClass('controls__action--pulse');
	}
}

function startTimer(){
	initPause();
	showingAnswers = false;
	clearTimeout(timer);
	getPhrase();
}

function showAnswers(){
	pauseTimer();
	showingAnswers = true;
	$('.phrase__tip').addClass('phrase__tip--active');
}

function hideAnswers(){
	showingAnswers = false;
	$('.phrase__tip').removeClass('phrase__tip--active');
}

function getPhrase(){
	$('.phrase').html('');

	if(shuffledPhrases){
		var card = shuffledPhrases.shift();
	
		$('.phrase').append('<p class="phrase__subject">' + card.word + '</p>');
		for(var i = 0; i <= 2; i++){
			var tip = card.tips[i];
			if(tip.length > 0){
				$('.phrase').append('<p class="phrase__tip">' + tip + '</p>');
			}
		}

		if(!paused){
			timer = setTimeout(function(){
				getPhrase()
			},(scrollRate*1000));
		}

		if(shuffledPhrases.length == 0){
			initPhrases();
		} 
	}
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}




