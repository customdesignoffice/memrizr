var scrollRate = 3;
var storageName = 'phrases';
var phrases = [];
var shuffledPhrases = [];
var fontSize = 4;
var themeLight = false;

var subjectSizeMap = {
	0: '24px',
	1: '30px',
	2: '38px',
	3: '48px',
	4: '60px',
	5: '76px',
}

var tipSizeMap = {
	0: '9px',
	1: '12px',
	2: '15px',
	3: '19px',
	4: '24px',
	5: '30px',
}

var rateMap = {
	0: 'Paused',
	1: '1 second',
	2: '2 seconds',
	3: '3 seconds',
	4: '4 seconds',
	5: '5 seconds',
}

initSettings();

function initSettings(){
	if(initPhrases()){
		initLoader(true);
		setSettings();
		initTheme();
		initStyles();
		applyStyles();
		initSliders();
		initToggle();
		initMenu();
		applyRate();
	} else {
		initIntro();
		initLoader();
		initAdd();
		initMenu();
		setSettings();
		initTheme();
		initStyles();
		applyStyles();
		initSliders();
		initToggle();
		applyRate();
	}
}

function initLoader(init){
	$(".loader__logo").one("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(e){ 
		$('.loader').addClass('loader--hidden');
		if(init){
			initInteractions();
			initPause();
			getPhrase();
		}
	});
}

function initIntro(){
	var introduced = getValue('introduced');
	if(typeof introduced === 'undefined' || introduced === null || !introduced){
		$('.intro-slide').show();
		$('#slide').bind('click', function(e){
			$('.intro-slide').addClass('intro-slide--hidden');
			setValue('introduced', true);
		})
	}
}

function initAdd(){
	$('#skip').find('.fa').removeClass('fa-arrow-right');
	$('#skip').find('.fa').addClass('fa-plus controls__action--pulse');
	$('#skip').addClass('controls__action--pulse');
	$('#skip').bind('click', function(e){
		e.preventDefault();
		window.location = '/edit';
	});
}

function initPhrases(){
	phrases  = getArray(storageName);
	if(typeof phrases !== 'undefined' && phrases != null){
		var phrasesArray = objectToArray(phrases);
		if(!phrasesArray.length){
			return false;
		}
		shuffledPhrases = shuffleArray(phrasesArray);
		return true;
	} else {
		return false;
	}
}

function initStyles(){
	$('head').append('<style id="settings">');
}

function initTheme(){
	if(themeLight){
		$('body').addClass('theme--light');
	}
}

function initSliders(){
	$('.settings__input[name=size]').val(fontSize);
	$('.settings__input[name=rate]').val(scrollRate);

	$('.settings__input').bind('input', function(e){
		updateSettings($(this));
	});
}

function initToggle(){
	$('#theme_toggle').bind('click', function(e){
		e.preventDefault();
		$('body').toggleClass('theme--light');

		if(themeLight){
			themeLight = false;	
		} else {
			themeLight = true;
		}
		addToArray('settings', 'theme', themeLight);
	});
}

function initMenu(){
	$('.tools__link--menu').bind('click', function(e){
		e.preventDefault();
		$('.menu').toggleClass('active');
	})
}

function applySettings(){
		applyStyles();
		applyRate();
}

function applyRate(){
	$('#rate_text').html(rateMap[scrollRate]);

	if(typeof paused !== 'undefined'){
		if(scrollRate != 0 && paused){
			startTimer();
		} else if (scrollRate == 0) {
			pauseTimer();
		}
	}	
}

function applyStyles(){
	var styles = '.phrase__subject{';
		styles += 'font-size:' + subjectSizeMap[fontSize]
		styles += '}';
		styles += '.phrase__tip{';
		styles += 'font-size:' + tipSizeMap[fontSize]
		styles += '}';

	$('#settings').append(styles);
}

function updateSettings(input){
	var value = input.val();
	var name = input.attr('name');

	addToArray('settings', input.attr('name'), input.val());

	setSettings();
	applySettings();
}

function setSettings(){
	var size = getFromArray('settings', 'size');
	var rate = getFromArray('settings', 'rate');
	var theme = getFromArray('settings', 'theme');
	console.log(theme);

	if(rate != null || typeof rate === 'undefined'){
		scrollRate = rate;
	}

	if(size != null || typeof size === 'undefined'){
		fontSize = size;
	}

	if(theme != null || typeof theme === 'undefined'){
		themeLight = theme;
	}
}

function objectToArray(object){
	return $.map(object, function(value, index) {
	    return [value];
	});
}

function shuffleArray(array){
	for(var j, x, i = array.length; i; j = Math.floor(Math.random() * i), x = array[--i], array[i] = array[j], array[j] = x);
    return array;
}