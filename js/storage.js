var localStorageSupported = false;

try {
	if ('localStorage' in window && window['localStorage'] !== null) {
		localStorageSupported = true;
	}
} catch (e) {
}

function setValue(key, value, json) {
	if (json) {
		value = JSON.stringify(value);
	}

	if (localStorageSupported) {
		var localStorage = window['localStorage'];
		localStorage.setItem(key, value);
	} else {
		var now = new Date();
		$.cookie(key, value, { 
			path: '/',
			expires: new Date(now.getFullYear(), (now.getMonth() + 3), now.getDate(), now.getHours(), now.getMinutes(), 00), 
			secure: isSecure
		});
	}
}

function getValue(key, json) {
	if (localStorageSupported) {
		var localStorage = window['localStorage'];
		var value = localStorage.getItem(key);

		if (json) {
			value = JSON.parse(value);
		}

		return value;

	} else {
		return JSON.parse($.cookie(key));
	}
}

function removeValue(key) {
	if (localStorageSupported) {
		var localStorage = window['localStorage'];
		localStorage.removeItem(key);
	} else {
		$.cookie(key, null, { 
			path: '/',
			secure: isSecure
		});
	}
}

function addToArray(arrayName, key, value) {
	array = getArray(arrayName);
	if (!array) {
		array = {};
	}

	// Add the new value into the array.
	array[key] = value;

	// Save it back again.
	if (localStorageSupported) {
		localStorage.setItem(arrayName, JSON.stringify(array));
	} else {
		var now = new Date();
		$.cookie(arrayName, JSON.stringify(array), { 
			path: '/',
			expires: new Date(now.getFullYear(), (now.getMonth() + 3), now.getDate(), now.getHours(), now.getMinutes(), 00), 
			secure: isSecure
		});
	}
}

function removeArray(arrayName) {
	removeValue(arrayName);
}

function removeFromArray(arrayName, key) {
	var array = getArray(arrayName);
	if (!array) {
		array = {};
	}

	// Remove the old value from the object.
	delete array[key];

	// Store the array again.
	if (localStorageSupported) {
		localStorage.setItem(arrayName, JSON.stringify(array));
	} else {
		var now = new Date();
		$.cookie(arrayName, JSON.stringify(array), { 
			path: '/',
			expires: new Date(now.getFullYear(), (now.getMonth() + 3), now.getDate(), now.getHours(), now.getMinutes(), 00), 
			secure: isSecure
		})
	}
}

function getArray(arrayName) {
	if (localStorageSupported) {
		var arrayJSON = window['localStorage'].getItem(arrayName);
		if (arrayJSON) {
			return JSON.parse(arrayJSON);
		}
	} else {
		return JSON.parse($.cookie(arrayName));
	}

	return null;
}

function getFromArray(arrayName, key) {
	var array = getArray(arrayName);
	if (!array) {
		return null;
	}

	if (!(key in array)) {
		return null;
	}

	return array[key];
}
